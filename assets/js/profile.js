console.log("hello World");
//get the access token in the local storage
let token = localStorage.getItem("token")
console.log(token)

let profile = document.querySelector('#profileContainer')

//lets create a control structure that will determine the display if the access token is null or empty.
if(!token || token === null) {
	//let redirect the user to the login page
	alert("You must Login First");
	window.location.href="./courses.html"



} else {
	// console.log("Yes! May nakuhang token") //for educationl purposes only
	fetch(`http://localhost:4000/api/users/details`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json())
	.then(data => {
		console.log(data); //checking purposes
		profile.innerHTML = `
			<div class="user-infos">
				<section class="jumbotron my-5">
					<h3 class="text-center">First Name: ${data.firstName}</h3>
					<h3 class="text-center">Last Name: ${data.lastName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
				</section>
			</div>
			<table class="enrolled-courses">
				<thead class="table">
					<tr class="table-infos">
						<th>Course Id: </th>
						<th>Enrolled On: </th>
						<th>Status: </th>
						<tbody></tbody>
					</tr>
				</thead>
			</table>
				



		`
	})
}