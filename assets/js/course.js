console.log('Hello from the other side');
//the first thing that we need to do is to identify which is need to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//window - represents browser's window.
//window.location - returns a location object with information about the current location of the document.
//.search - contains the query string section of the current.
//URL. //search property returns an object of type stringString.
//URLSearchParams() - this method/constructor creates and returnss a new URLSearchParans object. (this is a class).
//"URLSearchParams" - describes the interface that defines utility methods to work with query string of a URL. (this is the prop type).
//new -> insatantiate a user-defiend object.
//new - - also creates new object

/* params = {
	"courseId": "...id ng course that we passed"
   }
*/
let id = params.get('courseId')
console.log(id)

//lets capture the sections of the html body

let cName = document.querySelector("#courseName")
let cDesc = document.querySelector("#courseDesc")
let cPrice = document.querySelector("#coursePrice")

fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data) //if the result is displayed in the console, it means you were able to properly pass the course id and successfully created your fetch request.
	cName.innerHTML = data.name
	cDesc.innerHTML = data.description
	cPrice.innerHTML = data.price
}) 