//Clear/Wipe-out all the data inside our local storage so that the session of he user will end.
localStorage.clear()
//the clear method will allow us to remove the contents of the storage object.

//ONCE cleared out : redirect the user to the login page just incase a new user wants to log in.
window.location.replace('../index.html');