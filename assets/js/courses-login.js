console.log("hello World");

let loginForm = document.querySelector('#loginUser')

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();
	let email = document.querySelector("#userLoginEmail").value
	console.log(email)
	let password = document.querySelector("#password").value
	console.log(password)

//NOW: how can we inform a user that a blank input field cannot be logged in?
	if(email === "" || password === "") {
		alert("Please input your email and/or password.")
	} else {
		fetch('http://localhost:4000/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => {
			return res.json();
		}).then(data => {
			console.log(data.access)
			if(data.access) {
				//lets save the access token inside our local storage
				localStorage.setItem('token', data.access) //for saving to the localStorage
				//this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
				// \AppData\Local\Google\Chrome\User Data\Default\Local Storage
				alert('Access key saved on local storage.') //for educational purposes to see if succesfully saved on local storage.
				fetch(`http://localhost:4000/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data)
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					console.log("items are set inside the local storage.")
					//we are trying to direct the usr to the courses page upon successful login.SS
					// window.location.replace('./index.html')
					window.location.replace('./courses.html')
				})
			} else {
				//if there is no existing access key value from the data variable then just inform the user.
				alert('Something went wrong, Check your credentials')
			}
		})
	}

})