// console.log('hello')

//advance task answer key:
let courseForm = document.querySelector('#createCourse');

courseForm.addEventListener("submit", (e) => {
	e.preventDefault(); //which is use to avoid automatic page redirection.

	let cName = document.querySelector('#courseName').value;
		// console.log(cName); //console.log() - is just to check if it works
		let pName =  document.querySelector('#coursePrice').value;
		// console.log(pName);
		let cDesc = document.querySelector('#courseDescription').value;
		// console.log(cDesc);


		if((cName !== "") && (pName !== "") && (cDesc !== "")) {
			fetch('http://localhost:4000/api/courses/courseName-exists', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					name: cName
				})
			}).then(res => res.json())

		// console.log(res)
		.then(data => {
			if(data === false) {

				fetch('http://localhost:4000/api/courses/addCourse', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: cName,
						price: pName,
						description: cDesc
					})
				}).then(res => {
						return res.json() //after describing the structure of the request body, now create the structure of the response coming from the backend.
					}).then(info => {
						if(info === true) {
							alert('A New Course is Successfully Created')
						} else {
							alert('Something Went Wrong when adding a new course')
						}
					})
				} else {
					alert("Course already exists");
			}
		})
	} else {
		alert("Something went wrong pls check credentials")
	}
})
			// console.log(data)


			// let existingName = req.body.name.split("").map(letter => letter.tolowerCase());
			// cName.innerHTML = data.name.split("").map(letter => letter.toLowerCase());
			// existingName.length = cName.innerHTML;
			// 	// existingName.forEach(function(a) {
			// 	// })
			// 	existingName.forEach((a) => {
			// 	})
			// 	addedName.forEach((b) => {
			// 	})
			// existingName = existingName.join("")
			// addedName = addedName.join("")
			// if(addedName === existingName) {
			// 	console.log("Already Exists")
			// } else {
			// 	console.log("Succesfull")
			// }