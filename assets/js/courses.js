console.log("hello world")
let modalButton = document.querySelector('#adminButton');
//capture the html body which will display the content coming from the db.
let container = document.querySelector("#coursesContainer")
let cardFooter;

//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem('isAdmin')

if(isAdmin == 'false' || !isAdmin) {
	//if a user is a regular user, do not show the addcourse button.
	modalButton.innerHTML = null;
} else {
	modalButton.innerHTML = `
		<div class="courses">
			<a class="addCoursesButton">Add Course</a>
		</div>
	`
}


fetch('http://localhost:4000/api/courses/').then(res => res.json()).then(data => {
	console.log(data);
	//declare a variable that will display a result in the browser depending on the return
	let courseData;
	//create a control struture that will determine the value that the variable will hold.
	if(data.length < 1) { //ito na yung nasa controller na isActive ay true. Tapos kung walang mahanap isActive or panay false ito display niya >>
		courseData = "No Course Available"
	} else {
		//we will iterate the courses collection and display each course inside the browser
		courseData = data.map(course => {
		//lets check the make up of each element inside the courses collection
		console.log(course._id);

		//control structure that will determine if it's regular user or admin user. HERE: will display the enroll button and display course button.
		if(isAdmin =='false' || !isAdmin)
		{
			cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View course details</a> 

			<a href="#" class="btn btn-success text-white btn-block">Enroll</a>
			`
		} else {
			cardFooter = ` 
			<a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block"> Edit </a>
			<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block"> Disable Course </a>
			 `
		}

		return (
			`
			<div id="generated-courses-container">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							${course.name}
						</h5>
						<p class="card-text">
							<span>Description:</span> ${course.description}
						</p>
						<p class="card-text">
							<span>Price:</span> ${course.price}
						</p>
						<p class="card-text">
							<span>Created On:</span> ${course.createdOn}
						</p>
					</div>
					<div class="card-footer">
						${cardFooter}
					</div>
				</div>
			</div>
			`
			) //we attach a query string in the URL()${coures._id} which allows us to embed the ID from the database record into the query string.

			//the question mark(?) character inside the URL "acts" as a "separator", it indicates the end of a URL resource path, and indicates the start of the *query string*.

			//hashtag(#) - this was originally used to jump to an specific element with same id name/value
		}).join("") //we use the join() method to create a return of a new string.
		//it "concatinated" all the array objects inside the array and converted each to a string data type.
	}
	container.innerHTML = courseData;
})