console.log('Hello from the other side')
//capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession'); //for courses page. show login if user is not logged in and show logout if user logged in.

let regItem = document.querySelector('#regItem')

let profileItem = document.querySelector('#profileItem');

//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
console.log(userToken)

//lets create a control structure that will determine which elements inside the nav bar will be displayed if a user token is found in the local storage.

//courses page
if(!userToken) {
	navItem.innerHTML = 
		`
				<a class="loginButton" id="btn-border-underline">Log in</a>
		`
} else {
	navItem.innerHTML = 
		`
				<a href="./pages/logout.html" class="nav-link" id="btn-border-underline">Log Out</a>
		`
}

//show register if logged in

if(!userToken) {
	regItem.innerHTML =
	`
		<a class="regButton" id="btn-border-underline">Register</a>
	`
}

//show profile if logged in

if(userToken) {
	profileItem.innerHTML = 
	`
		<a href="./pages/profile.html" class="profileButton" id="btn-border-underline">Profile</a>
	`
}