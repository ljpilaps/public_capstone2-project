console.log('Hello from the other side')
//capture the navSession element inside the navbar component
let navItem = document.querySelector('#navSession'); //for courses page. show login if user is not logged in and show logout if user logged in.

let homeItem = document.querySelector('#homeItem')



//lets take the access token from the local storage property.
let userToken = localStorage.getItem("token")
console.log(userToken)

//lets create a control structure that will determine which elements inside the nav bar will be displayed if a user token is found in the local storage.

//courses page
if(userToken) {
	navItem.innerHTML = 
		`
				<a href="./logout.html" class="nav-link" id="btn-border-underline">Log Out</a>
		`
}

if(userToken) {
	homeItem.innerHTML = 
	`
		<a href="../index.html" class="nav-link" id="btn-border-underline">Home</a>
	`
}