/* login modal*/
let modalBtn = document.querySelector('.loginButton')
let modalBg = document.querySelector('.overlay-login-bg')
let modalClose = document.querySelector('.login-close-btn')

modalBtn.addEventListener('click', () => {
	modalBg.classList.add('login-bg-active');	
})

modalClose.addEventListener('click', () => {
	modalBg.classList.remove('login-bg-active');
})