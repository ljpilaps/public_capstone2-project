let addCourseBtn = document.querySelector('.addCoursesButton');
let addCourseBg = document.querySelector('.add-course-overlay-bg');
let modaladdCourseClose = document.querySelector('.close-btn');

addCourseBtn.addEventListener('click', () => {
	addCourseBg.classList.add('add-course-overlay-bg-active');
})

modaladdCourseClose.addEventListener('click', () => {
	addCourseBg.classList.remove('add-course-overlay-bg-active');
})