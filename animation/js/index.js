console.log('hrllo world')
/* SLIDESHOW */
var slideNo = 1;


Carousel = (n) => {
	let slides = document.querySelectorAll('.image-container');
	let dots = document.querySelectorAll('.dot');
	if(n > slides.length) {
		slideNo = 1;
	}
	if(n < 1) {
		slideNo = slides.length;
	}
	for(var i= 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	slides[slideNo - 1].style.display = "block";
	for(var i= 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	dots[slideNo - 1].className += " active";
}

Carousel(slideNo);

newSlide = (n) => {
	Carousel(slideNo += n);
}

currentSlide = (n) => {
	Carousel(slideNo = n);
}

//navbar collapse
const menuBtn = document.querySelector('.menu-btn');
let menuOpen = false;
menuBtn.addEventListener('click', () => {
    if(!menuOpen) {
        menuBtn.classList.add('open');
        menuOpen = true;
    } else { 
        menuBtn.classList.remove('open');
        menuOpen = false;
    }
});


//Navbar smallphones
const toggleButton = document.querySelector('.menu-btn')
const navBarNav = document.querySelector('.navbar-nav')

toggleButton.addEventListener('click', () => {
	navBarNav.classList.toggle('active');
})